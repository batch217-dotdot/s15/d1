console.log("Mabuhay!");

// JS - is a loosely type Programming Language

alert("Hello Again");

console. log ( "Hello World" ) ;

console.
log
(
	"Hello World"
);

//[SECTION] - Making Comments in JS

/*
	1. Single-line comment - Ctrl + /
	2. Multi-line comment - Ctrl + Shift + /
*/

// [SECTION] - variables
// used to contain data
// usually stored in a computer's memory

// declaration of variable

//syntax --> let variablename;
let myVariable;

console.log(myVariable);

// syntax --> let variablename variablevalue
let mySecondVariable = 2;
console.log(mySecondVariable);

let productName = "desktop computer";
console.log(productName);

// reassigning value

let friend;

friend = "Kate";
friend = "Jane";
friend = "Russel";
console.log(friend);

// syntax const variablename variablevalue

const pet = "Bruno";
// pet = "Lala";
console.log(pet);

// local and global variables

let outerVariable = "hello"; // global variable

{
	let innerVariable = "Hello World!" //Local variable
	console.log(innerVariable);
}

console.log(outerVariable);

const outerExample = "Global Variable";

{
	const innerExample = "Local Variable";
	console.log(innerExample);
}

console.log(outerExample);


// Multiple Declaration

let productCode = "DC017", productBrand = "Dell";

// let productCode = "DC017";
// let productBrand = "Dell";

console.log(productCode, productBrand);

//[Section] Data Types

//strings

let country = 'Philippines';
let province = "Metro Manila";

// Concatenating strings
// Multiple string values can be combined to create a single string using the "+" symbol

let fullAddress = province + ", " + country;
console.log(fullAddress);

let greeting = "I live in" + ", " + country;
console.log(greeting);

// The escape character (\) in strings in combination with other characters can produce different effects

let mailAddress = "Metro Manila\n\nPhilippines";
console.log(mailAddress);

let message = "John\"s employees went home early";
console.log(message);

//numbers
let headcount = 26;
console.log(headcount);

let grade = 98.7;
console.log(grade);

let planetDistance = 2e10;
console.log(planetDistance);

console.log("My grade last sem is " + grade);

// Boolean
let isMarried = false;
let inGoodConduct = true;

console.log("isMarried: " + isMarried);
console.log("isGoodConduct: " + inGoodConduct);

// arrays
let grades = [98.7, 92.2, 90.2, 94.6];
console.log(grades);

let details = ["John", "Smith", 32, true];
console.log(details);

//objects

let person = {
	fullName: "Juan Dela Cruz",
	age: 35,
	isMarried: false,
	contacts: ["09123456789", "099999999"],
	address: { //nested object
		houseNumber: "345",
		city: "Manila"
	}
}
console.log(person);

//re-assigning value to an array
const anime = ["one piece", "one punch man", "attack on titan"];
anime[1] = "kimetsu no yaiba";
console.log(anime);